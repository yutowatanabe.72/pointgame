<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ScorerankingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ScorerankingTable Test Case
 */
class ScorerankingTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ScorerankingTable
     */
    public $Scoreranking;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Scoreranking'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Scoreranking') ? [] : ['className' => ScorerankingTable::class];
        $this->Scoreranking = TableRegistry::getTableLocator()->get('Scoreranking', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Scoreranking);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
