<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Scoreranking[]|\Cake\Collection\CollectionInterface $scoreranking
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Scoreranking'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="scoreranking index large-9 medium-8 columns content">
    <h3><?= __('Scoreranking') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Score') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($scoreranking as $scoreranking): ?>
            <tr>
                <td><?= $this->Number->format($scoreranking->Id) ?></td>
                <td><?= $this->Number->format($scoreranking->Score) ?></td>
                <td><?= h($scoreranking->Date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $scoreranking->Id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $scoreranking->Id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $scoreranking->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $scoreranking->Id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
