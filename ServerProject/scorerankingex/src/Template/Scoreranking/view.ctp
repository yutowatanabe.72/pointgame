<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Scoreranking $scoreranking
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Scoreranking'), ['action' => 'edit', $scoreranking->Id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Scoreranking'), ['action' => 'delete', $scoreranking->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $scoreranking->Id)]) ?> </li>
        <li><?= $this->Html->link(__('List Scoreranking'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Scoreranking'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="scoreranking view large-9 medium-8 columns content">
    <h3><?= h($scoreranking->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($scoreranking->Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Score') ?></th>
            <td><?= $this->Number->format($scoreranking->Score) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($scoreranking->Date) ?></td>
        </tr>
    </table>
</div>
