<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Scoreranking Controller
 *
 * @property \App\Model\Table\ScorerankingTable $Scoreranking
 *
 * @method \App\Model\Entity\Scoreranking[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ScorerankingController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $scoreranking = $this->paginate($this->Scoreranking);

        $this->set(compact('scoreranking'));
    }

    /**
     * View method
     *
     * @param string|null $id Scoreranking id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $scoreranking = $this->Scoreranking->get($id, [
            'contain' => []
        ]);

        $this->set('scoreranking', $scoreranking);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $scoreranking = $this->Scoreranking->newEntity();
        if ($this->request->is('post')) {
            $scoreranking = $this->Scoreranking->patchEntity($scoreranking, $this->request->getData());
            if ($this->Scoreranking->save($scoreranking)) {
                $this->Flash->success(__('The scoreranking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The scoreranking could not be saved. Please, try again.'));
        }
        $this->set(compact('scoreranking'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Scoreranking id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $scoreranking = $this->Scoreranking->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $scoreranking = $this->Scoreranking->patchEntity($scoreranking, $this->request->getData());
            if ($this->Scoreranking->save($scoreranking)) {
                $this->Flash->success(__('The scoreranking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The scoreranking could not be saved. Please, try again.'));
        }
        $this->set(compact('scoreranking'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Scoreranking id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $scoreranking = $this->Scoreranking->get($id);
        if ($this->Scoreranking->delete($scoreranking)) {
            $this->Flash->success(__('The scoreranking has been deleted.'));
        } else {
            $this->Flash->error(__('The scoreranking could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getScore(){
        error_log("getScore()");
        $this->autoRender = false;
        
        $query = $this->Scoreranking->find('all');
              $query->order(['score' => 'DESC']);       //カラム['id']をキーにして昇順ソート
              $query->limit(3);                    //表示個数を3つに絞る    
        //クエリを実行してarrayにデータを格納
        $json_array = json_encode($query);
        //---------------
        // $json_array の内容を出力
        echo $json_array;
    }
    public function setScore(){
        error_log("setScore()");
        $this->autoRender = false;
        $score = 0;
        if( isset( $this->request->data['Score'] ) ){
            $score   = $this->request->data['Score'];
            error_log($score);
        }
       
        $data   = array ( 'Score' => $score,'Date' => date('Y/m/d H:i:s') ); //timestamp型
        $scoreRanking = $this->Scoreranking->newEntity();
        $scoreRanking = $this->Scoreranking->patchEntity($scoreRanking, $data);
        if ($this->Scoreranking->save($scoreRanking)) {
            //追加成功
            echo "success"; //success!
        }else{
            //追加失敗
            echo "failed"; //failed!
        }      
    }
}
