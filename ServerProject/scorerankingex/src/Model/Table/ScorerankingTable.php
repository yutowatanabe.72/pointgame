<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Scoreranking Model
 *
 * @method \App\Model\Entity\Scoreranking get($primaryKey, $options = [])
 * @method \App\Model\Entity\Scoreranking newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Scoreranking[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Scoreranking|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Scoreranking saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Scoreranking patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Scoreranking[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Scoreranking findOrCreate($search, callable $callback = null, $options = [])
 */
class ScorerankingTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('scoreranking');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('Id')
            ->allowEmptyString('Id', null, 'create');

        $validator
            ->integer('Score')
            ->requirePresence('Score', 'create')
            ->notEmptyString('Score');

        $validator
            ->dateTime('Date')
            ->notEmptyDateTime('Date');

        return $validator;
    }
}
