-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.6-MariaDB
-- PHP のバージョン: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `techtest`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `scoreranking`
--

CREATE TABLE `scoreranking` (
  `Id` int(11) NOT NULL,
  `Score` int(11) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `scoreranking`
--

INSERT INTO `scoreranking` (`Id`, `Score`, `Date`) VALUES
(1, 10, '2019-09-29 23:05:57'),
(2, 30, '2019-09-29 23:05:57'),
(3, 50, '2019-09-29 23:05:57'),
(4, 0, '2019-09-29 14:06:08'),
(21, 0, '2019-09-30 00:56:40'),
(22, 0, '2019-09-30 00:59:29'),
(23, 0, '2019-09-30 01:01:18'),
(24, 0, '2019-09-30 01:01:36'),
(25, 0, '2019-09-30 01:02:00'),
(26, 0, '2019-09-30 01:26:34'),
(27, 0, '2019-09-30 01:54:44'),
(28, 350, '2019-09-30 01:55:14'),
(29, 0, '2019-09-30 01:56:32'),
(30, 760, '2019-10-01 18:55:38'),
(31, 0, '2019-10-03 03:06:28'),
(32, 120, '2019-10-03 03:07:18'),
(33, 0, '2019-10-03 03:07:45'),
(34, 1020, '2019-10-15 06:43:51'),
(35, 980, '2019-10-15 06:45:57'),
(36, 1430, '2019-10-15 06:49:41'),
(37, 150, '2019-10-15 06:51:11'),
(38, 640, '2019-10-15 06:54:08');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `scoreranking`
--
ALTER TABLE `scoreranking`
  ADD PRIMARY KEY (`Id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `scoreranking`
--
ALTER TABLE `scoreranking`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
