﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private Vector3 playerPositon;

    public Slider slider;

    // Start is called before the first frame update
    void Start()
    {
        playerPositon = transform.position;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void MovePlayer()
    {
        Vector2 position = transform.position;
        position.x = playerPositon.x + slider.value - (slider.maxValue / 2);
        transform.position = position;
    }
}
