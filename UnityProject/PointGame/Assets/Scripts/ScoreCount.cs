﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCount : MonoBehaviour
{
    [HideInInspector] public static int Score;
    //public static int LastScore;
    public Text ScoreText;
    
    // Start is called before the first frame update
    void Start()
    {
        Score = 0;
        //LastScore = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameJudge.end == false)
        {
            Score = Player.scoreCount * 10 +
                Player.scoreCoun2 * 30 + Player.scoreCoun3 * 50;

            ScoreText.text = "SCORE:" + Score.ToString();
        }
        //else
        //{
        //    LastScore = Score;
        //}
    }
}
