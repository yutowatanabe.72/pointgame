﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpBar : MonoBehaviour
{
    public static float playerhp = 500;
    float defoultLife = playerhp;
    Slider slider;

    // Start is called before the first frame update
    void Start()
    {
        slider = GameObject.Find("LifeGage").GetComponent<Slider>();
        slider.maxValue = defoultLife;
    }

    // Update is called once per frame
    void Update()
    {
        playerhp = defoultLife - (Plate.itemCount * 10 + Plate.itemCount2 * 30 + Plate.itemCount3 * 50);
        Debug.Log(playerhp);
        slider.value = playerhp;
    }
}
