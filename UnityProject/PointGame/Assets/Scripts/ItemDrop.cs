﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDrop : MonoBehaviour
{
    public GameObject Item1;
    public GameObject Item2;
    public GameObject Item3;
    public GameObject Disturb;

    float span = 0.3f;
    float delta = 0;

    public Transform rootCanvas;

    // Update is called once per frame
    void Update()
    {
        if (StartButton.start) {
            if (GameJudge.end == false)
            {
                this.delta += Time.deltaTime;

                int num = Random.Range(0, 10);

                if (this.delta > this.span)
                {
                    this.delta = 0;
                    if (num >= 8)
                    {
                        GameObject Item = Instantiate(Disturb);
                        Item.transform.SetParent(rootCanvas);
                        Item.transform.position = new Vector2(Random.Range(125, 475), 600);
                    }
                    else if (num >= 4)
                    {
                        GameObject Item = Instantiate(Item1);
                        Item.transform.SetParent(rootCanvas);
                        Item.transform.position = new Vector2(Random.Range(125, 475), 600);
                    }
                    else if (num >= 1)
                    {
                        GameObject Item = Instantiate(Item2);
                        Item.transform.SetParent(rootCanvas);
                        Item.transform.position = new Vector2(Random.Range(125, 475), 600);
                    }
                    else
                    {
                        GameObject Item = Instantiate(Item3);
                        Item.transform.SetParent(rootCanvas);
                        Item.transform.position = new Vector2(Random.Range(125, 475), 600);
                    }
                }
            }
        }
    }
}
