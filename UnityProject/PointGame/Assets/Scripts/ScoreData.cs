﻿public class ScoreData
{
    public long Score { get; set; }

    public ScoreData()
    {
        Score = 0;
    }
}