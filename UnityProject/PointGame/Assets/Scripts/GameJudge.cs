﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameJudge : MonoBehaviour
{
    public static bool end;
    public Text ResultText;
    public Text ResultScore;

    public GameObject GameUI;
    public GameObject resultUI;
    public GameObject Button;
    public GameObject Button2;
    public GameObject Button3;
    public GameObject Score;

    // Start is called before the first frame update
    void Start()
    {
        end = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (StartButton.start)
        {
            if (HpBar.playerhp <= 0)
            {
                end = true;
                Debug.Log(end);
            }

            if (end)
            {
                GameUI.SetActive(false);
                resultUI.SetActive(true);
                Button.SetActive(true);
                Button2.SetActive(true);
                Button3.SetActive(true);
                Score.SetActive(true);

                ResultScore.text = "ResultScore:" + ScoreCount.Score;
            }
        }
    }
}
