﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartButton : MonoBehaviour
{
    public static bool start = false;
    public GameObject GameUI;
    public void OnClick()
    {
        start = true;
        GameUI.SetActive(true);
        gameObject.SetActive(false);
    }
}
