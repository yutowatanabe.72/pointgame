﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plate : MonoBehaviour
{
    public static int itemCount = 0;
    public static int itemCount2 = 0;
    public static int itemCount3 = 0;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Item")
        {
            itemCount++;
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag == "Item2")
        {
            itemCount2++;
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag == "Item3")
        {
            itemCount3++;
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag == "Disturb")
        {
            Destroy(collision.gameObject);
        }
    }
}
